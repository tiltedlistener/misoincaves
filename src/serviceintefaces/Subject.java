package serviceintefaces;

import command.Command;

public interface Subject {
	public void addObserver(Observer obs);
	public void removeObserver(Observer obs);
	public void notifyObservers(Command command);
}
