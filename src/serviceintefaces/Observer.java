package serviceintefaces;

import command.Command;

public interface Observer {
	public void onNotify(Command command);
}
