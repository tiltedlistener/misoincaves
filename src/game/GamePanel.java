package game;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class GamePanel extends JPanel {
	
	// Window
	private int screenWidth;
	private int screenHeight;
	private Game parent;
	
	// Graphics
	private Graphics2D g2d;
	public Graphics2D graphics() { return g2d; }
	private BufferedImage backBuffer;		
	
	public GamePanel(int _screenwidth, int _screenheight, Game _parent) {
		screenWidth = _screenwidth;
		screenHeight = _screenheight;

		parent = _parent;
		backBuffer = new BufferedImage(screenWidth, screenHeight, BufferedImage.TYPE_INT_RGB);
		g2d = backBuffer.createGraphics();		
	}
	
	public void paint(Graphics g) {
		g2d.clearRect(0, 0, screenWidth, screenHeight);	
		parent.panelRender();
		g.drawImage(backBuffer, 0, 0, this);
	}
	
}
