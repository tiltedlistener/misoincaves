package game;

import java.util.ArrayList;

import particles.ParticleGenerator;
import physics.Vector2D;
import entities.*;
import strategy.*;
import factories.GameEntityFactory;

public class MisoInCaves extends Game {
	private static final long serialVersionUID = 1L;
	
	// Factories
	private GameEntityFactory entityFactory;
	
	// Scenes
	private SceneGraph sceneGraph;
	private Camera camera;

	public MisoInCaves(int _screenwidth, int _screenheight, String _title) {
		super(_screenwidth, _screenheight, _title);
		sceneGraph = new SceneGraph();
		camera = new Camera(screenWidth, screenHeight);
		entityFactory = new GameEntityFactory(g2d, input, sceneGraph, camera);

		PlayerEntity hero;
		ArrayList<WallEntity> wall1;
		ArrayList<WallEntity> wall2;
		ArrayList<WallEntity> wall3;
		ArrayList<WallEntity> wall4;		
		GameEntity follower;
		hero = (PlayerEntity) entityFactory.createPlayer();
		hero.setPosition(new Vector2D(200, 100));		
		hero.setVelocity(new Vector2D(0, 0));

		wall1 = entityFactory.createWallLine(false, 9, 0, 0);
		wall2 = entityFactory.createWallLine(false, 9, 600, 0);
		wall3 = entityFactory.createWallLine(true, 10, 51, 0);
		wall4 = entityFactory.createWallLine(true, 10, 51, 450);

		follower = (FollowingEntity) entityFactory.createFollowing();
		follower.setPosition(new Vector2D(400, 100));		
		follower.setVelocity(new Vector2D(0, 0));	
		((FollowingEntity) follower).addFollowStrat(new MoveLeftStrategy());

		WallEntity wall = entityFactory.createWall();
		wall.setPosition(new Vector2D(100, 100));
		sceneGraph.addToGraph(wall);
		
		sceneGraph.addGroupToGraph(wall1);
		sceneGraph.addGroupToGraph(wall2);
		sceneGraph.addGroupToGraph(wall3);
		sceneGraph.addGroupToGraph(wall4);		

		sceneGraph.addToGraph(hero);
		hero.addObserver(camera);
		
		start();
	}

	@Override
	public void render(double interpolation) {
		sceneGraph.render(interpolation);
	}

	@Override
	public void update() {
		sceneGraph.update();
	}
	
}
