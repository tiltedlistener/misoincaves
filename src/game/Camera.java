package game;

import command.Command;

import physics.Vector2D;
import serviceintefaces.*;

public class Camera implements Observer {

	private Vector2D position;
	private double width;
	private double height;
	
	public Camera(double _width, double _height) {
		position = new Vector2D();
		width = _width;
		height = _height;
	}
	
	public Vector2D getPosition() {
		return position;
	}
	
	public Vector2D getRange() {
		return new Vector2D(width, height);
	}
	
	public void updatePosition(Vector2D _update){
		position.add(_update);
	}

	@Override
	public void onNotify(Command command) {
		if (command.getEvent() == Event.PLAYER_MOVED) {
			Vector2D result = command.getVector2D();
			updatePosition(result);
		}
	}
	
}
