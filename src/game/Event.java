package game;

public enum Event {
	
	// Ray Events
	RAY_HIT,
	
	// Player Events
	PLAYER_MOVED,
}
