package game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

import physics.*;
import entities.*;

public class SceneGraph {

	private LinkedList<GameEntity> gameEntities = new LinkedList<GameEntity>();
	
	public void addToGraph(GameEntity entity) {
		gameEntities.add(entity);
	}
	
	public void addGroupToGraph(ArrayList<WallEntity> walls) {
		Iterator<WallEntity> iterator = walls.iterator();
		while (iterator.hasNext()) {
			gameEntities.add((GameEntity) iterator.next());
		}
	}
	
	public void render(double interpolation) {
		ListIterator<GameEntity> listIterator = gameEntities.listIterator();
        while (listIterator.hasNext()) {
        	listIterator.next().render(interpolation);
        }			
	}	
	
	public void update() {
		ListIterator<GameEntity> listIterator = gameEntities.listIterator();
        while (listIterator.hasNext()) {
        	listIterator.next().update();
        }		
		checkCollisions();
	}	
	
	public void checkCollisions() {	
		int size = gameEntities.size();
		for (int i = 0; i < size - 1; i++) {
			GameEntity outerEntity = gameEntities.get(i);
			for (int j = i+1; j < size; j++) {
				GameEntity innerEntity = gameEntities.get(j);
				
				if (outerEntity.isMoving()) {
					Sweep result = sweep(outerEntity, innerEntity);
					outerEntity.resolveCollision(result);
				}
				if (innerEntity.isMoving()) {
					Sweep result = sweep(innerEntity, outerEntity);
					innerEntity.resolveCollision(result);
				}
			}
		}	
	}
	
	public Sweep sweep(GameEntity movingEntity, GameEntity staticEntity) {
	    double xInvEntry = 0, yInvEntry = 0;
	    double xInvExit = 0, yInvExit = 0;
		
	    double xEntry, yEntry;
	    double xExit, yExit;	    
	    
		if (movingEntity.getVelocity().X() > 0.0) {
	        xInvEntry = staticEntity.getPosition().X() - (movingEntity.getPosition().X() + movingEntity.getSize().X());
	        xInvExit = (staticEntity.getPosition().X() + staticEntity.getSize().X()) - movingEntity.getPosition().X();
		} else if (movingEntity.getVelocity().X() < 0.0) {
	        xInvEntry = (staticEntity.getPosition().X() + staticEntity.getSize().X()) - movingEntity.getPosition().X();
	        xInvExit = staticEntity.getPosition().X() - (movingEntity.getPosition().X() + movingEntity.getSize().X());			
		}
		
	    if (movingEntity.getVelocity().Y() > 0.0) {
	        yInvEntry = staticEntity.getPosition().Y() - (movingEntity.getPosition().Y() + movingEntity.getSize().Y());
	        yInvExit = (staticEntity.getPosition().Y() + staticEntity.getSize().Y()) - movingEntity.getPosition().Y();
	    } else if (movingEntity.getVelocity().Y() < 0.0) {
	        yInvEntry = (staticEntity.getPosition().Y() + staticEntity.getSize().Y()) - movingEntity.getPosition().Y();
	        yInvExit = staticEntity.getPosition().Y() - (movingEntity.getPosition().Y() + movingEntity.getSize().Y());
	    }	    
		
	    if (movingEntity.getVelocity().X() == 0.0) {
	        xEntry = Double.NEGATIVE_INFINITY;
	        xExit = Double.POSITIVE_INFINITY;
	    } else{
	        xEntry = xInvEntry / movingEntity.getVelocity().X();
	        xExit = xInvExit / movingEntity.getVelocity().X();
	    }	
	    
	    if (movingEntity.getVelocity().Y() == 0.0) {
	        yEntry = Double.NEGATIVE_INFINITY;
	        yExit = Double.POSITIVE_INFINITY;
	    } else {
	        yEntry = yInvEntry / movingEntity.getVelocity().Y();
	        yExit = yInvExit / movingEntity.getVelocity().Y();
	    }		    	     
	 
	    double entryTime = Math.max(xEntry, yEntry);
	    double exitTime = Math.min(xExit, yExit);	
	    
	    boolean overlapTest = false;
	    if (xEntry > yEntry) {
	    	overlapTest = overlappingRectangles(
				movingEntity.getPosition().Y(), movingEntity.getPosition().Y() + movingEntity.getSize().Y(),
				staticEntity.getPosition().Y(), staticEntity.getPosition().Y() +staticEntity.getSize().Y()
			);	
	    } else {
	    	overlapTest = overlappingRectangles(
				movingEntity.getPosition().X(), movingEntity.getPosition().X() + movingEntity.getSize().X(),
				staticEntity.getPosition().X(), staticEntity.getPosition().X() +staticEntity.getSize().X()
			);	    	
	    }
	    
	    Sweep result = new Sweep();
	    if (
	    		Math.abs(entryTime) > 1.0 || !overlapTest
	       ) 
	    {
	    	result.normal = new Vector2D(0, 0);
	    	result.time = 1;
	        return result;
	    }
	    
	    Vector2D normal;
	    if (xEntry > yEntry) {
	        if (xEntry < 0.0) {
	        	normal = new Vector2D(1, 0);
	        } else {
	        	normal = new Vector2D(-1, 0);	        	
	        }
	    } else {
            if (yEntry < 0.0) {
            	normal = new Vector2D(0, 1);
            } else {
            	normal = new Vector2D(0, -1);
            }	    	
	    }
        
        result.normal = normal;
        result.time = entryTime;       
	    
        return result;
	}
	
	private boolean overlappingRectangles(double minA, double maxA, double minB, double maxB) {
		return minB < maxA && minA < maxB;
	}	
	
	public GameEntity checkRayCollision(Ray ray) {
		ListIterator<GameEntity> listIterator = gameEntities.listIterator();
        while (listIterator.hasNext()) {
        	GameEntity entity = listIterator.next();
        	
        	if (!ray.isEntityMasked(entity)) {
	        	Rectangle2D rect = entity.getRectangle();
	        	Line2D sLine = new Line2D(ray.getPosition(), Vector2D.subVectors(ray.getEndpoint(), ray.getPosition()));
	        	
	        	if (!checkLineCollision(sLine, rect)) {
	        		return null;
	        	}
	        	
	        	Range rRange = new Range();
	        	Range sRange = new Range();
	        	Vector2D[] corners = entity.getRectangle().getCorners();
	        	
	        	rRange.setMin(corners[0].X());
	        	rRange.setMax(corners[1].X());
	        	sRange.setMin(ray.getPosition().X());
	        	sRange.setMax(ray.getEndpoint().X());
	        	sRange.sort();
	        	if (!Range.overlappingRanges(rRange, sRange)) 
	        		return null;
	        	
	        	rRange.setMin(corners[0].Y());
	        	rRange.setMax(corners[3].Y());
	        	sRange.setMin(ray.getPosition().Y());
	        	sRange.setMax(ray.getEndpoint().Y());
	        	sRange.sort();
	        	if (!Range.overlappingRanges(rRange, sRange)) 
	        		return null;	        	
	        	
	        	return entity;
        	}
        }	
        return null;
	}
	
	public boolean checkLineCollision(Line2D line, Rectangle2D rect) {
    	Vector2D n = Vector2D.rotateVector90(line.getDirection());
    	Vector2D[] corners = rect.getCorners();
    	
    	corners[0].sub(line.getBase());
    	corners[1].sub(line.getBase());
    	corners[2].sub(line.getBase());
    	corners[3].sub(line.getBase());
    	
    	double res1 = Vector2D.dotProduct(n, corners[0]);
    	double res2 = Vector2D.dotProduct(n, corners[1]);
    	double res3 = Vector2D.dotProduct(n, corners[2]);
    	double res4 = Vector2D.dotProduct(n, corners[3]);
    	
    	if ((res1 * res2 <= 0) || (res2 * res3 <= 0) || (res3 * res4 <= 0)) {
    		return true;
    	} else {
    		return false;
    	}
	}	
	
}
