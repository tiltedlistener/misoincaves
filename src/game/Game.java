package game;

import input.*;
import java.awt.*;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public abstract class Game extends JFrame implements Runnable {

	// Settings
	protected int screenWidth;
	protected int screenHeight;
	
	// Loop
	private Thread gameLoop;
	private int fps = 60;
	private int frameCount = 0;	
	
	// Controls
	protected InputHandler input = new InputHandler();
	
	// Graphics
	protected GamePanel graphicsPanel;
	protected Graphics2D g2d;
	protected float interpolation = 1;
	protected double epsilon = 1 / 30;
	
	// Game state
	protected boolean paused = false;
	
	// Abstract Methods
	public abstract void render(double interpolation);
	public abstract void update();

	
	public Game(int _screenwidth, int _screenheight, String _title) {
		super(_title);
		
		// Frame Settings
		screenWidth = _screenwidth;
		screenHeight = _screenheight;
		
		// Screen
		setSize(screenWidth, screenHeight);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setUndecorated(true);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);			
		
		// Panel
		graphicsPanel = new GamePanel(screenWidth, screenHeight, this);
		g2d = graphicsPanel.graphics();
		getContentPane().add(graphicsPanel);
		
		// Input
		addKeyListener(input);
	}
	
	public void start() {
		// Threads
		gameLoop = new Thread(this);
		gameLoop.start();
		
		// Begin
		setVisible(true);		
	}

	@Override
	public void run() {
		Thread t = Thread.currentThread();
		/**
		 * Credit to Eli Delvanthal for the loop structure
		 * http://www.java-gaming.org/index.php?topic=24220.0
		 */
		final double GAME_HERTZ = 30.0;
		
		// Calculate how many ns each frame should take for our target game hertz.
		final double TIME_BETWEEN_UPDATES = 1000000000 / GAME_HERTZ;
		
		// At the very most we will update the game this many times before a new render.
		final int MAX_UPDATES_BEFORE_RENDER = 5;
		
		//We will need the last update time.
		double lastUpdateTime = System.nanoTime();
		
		// Store the last time we rendered.
		double lastRenderTime = System.nanoTime();
		  
		// If we are able to get as high as this FPS, don't render again.
		final double TARGET_FPS = 60;
		final double TARGET_TIME_BETWEEN_RENDERS = 1000000000 / TARGET_FPS;
		
		//Simple way of finding FPS.
	    int lastSecondTime = (int) (lastUpdateTime / 1000000000);
		
		while(t == gameLoop) {
			double now = System.nanoTime();
			int updateCount = 0;
			 
			if (!paused) {
				//Do as many game updates as we need to, potentially playing catchup.
				while( now - lastUpdateTime > TIME_BETWEEN_UPDATES && updateCount < MAX_UPDATES_BEFORE_RENDER ) {
					update();
					lastUpdateTime += TIME_BETWEEN_UPDATES;
					updateCount++;
				}

				//If for some reason an update takes forever, we don't want to do an insane number of catchups.
				//If you were doing some sort of game that needed to keep EXACT time, you would get rid of this.
				if ( now - lastUpdateTime > TIME_BETWEEN_UPDATES) {
				   lastUpdateTime = now - TIME_BETWEEN_UPDATES;
				}

				//Render. To do so, we need to calculate interpolation for a smooth render.
				interpolation = Math.min(1.0f, (float) ((now - lastUpdateTime) / TIME_BETWEEN_UPDATES) );
				repaintPanel();
				lastRenderTime = now;

				//Update the frames we got.
				int thisSecond = (int) (lastUpdateTime / 1000000000);
				if (thisSecond > lastSecondTime) {
				   fps = frameCount;
				   
				   frameCount = 0;
				   lastSecondTime = thisSecond;
				}

				//Yield until it has been at least the target time between renders. This saves the CPU from hogging.
				while ( now - lastRenderTime < TARGET_TIME_BETWEEN_RENDERS && now - lastUpdateTime < TIME_BETWEEN_UPDATES) {
				   Thread.yield();

				   //This stops the app from consuming all your CPU. It makes this slightly less accurate, but is worth it.
				   //You can remove this line and it will still work (better), your CPU just climbs on certain OSes.
				   //FYI on some OS's this can cause pretty bad stuttering. Scroll down and have a look at different peoples' solutions to this.
				   try {Thread.sleep(1);} catch(Exception e) {} 

				   now = System.nanoTime();
				} 
			
			}			
		}			
	}

	public void stop() {
		gameLoop = null;
	}	

	public void repaintPanel() {
		graphicsPanel.repaint();
	}	
	
	public void panelRender() {
		render(interpolation);
	}
	
}
