package physics;

import java.util.ArrayList;
import java.util.Iterator;

import command.Command;

import entities.BaseEntity;
import entities.GameEntity;
import game.Event;
import serviceintefaces.*;

public class Ray extends BaseEntity implements Subject {
	
	protected double distance;
	protected Vector2D endPoint;		
	protected ArrayList<String> mask = new ArrayList<String>();		// Who to ignore if it hits
	protected ArrayList<Observer> observers = new ArrayList<Observer>();
	
	public void setDistance(double _distance) {
		distance = _distance;
	}
	
	public Vector2D getEndpoint() {
		// TODO allow for any position. Given a direction and distance compute endpoint
		return new Vector2D(pos.X() - distance, pos.Y());
	}
	
	public boolean isEntityMasked(GameEntity entity) {		
		if (mask.contains(entity.getClass().toString())) 
			return true;
		
		return false;
	}
	
	public void addMask(String type) {
		mask.add(type);
	}
	
	@Override
	public void update() {}	

	@Override
	public void addObserver(Observer obs) {
		observers.add(obs);
	}
	@Override
	public void removeObserver(Observer obs) {
		observers.remove(obs);
	}
	@Override
	public void notifyObservers(Command command) {
		Iterator<Observer> observerIterator = observers.iterator();
		while (observerIterator.hasNext()) {
			observerIterator.next().onNotify(command);
		}
	}
	
}
