package physics;

public class Line2D {
	public Vector2D base;
	public Vector2D direction;
	
	public Line2D(Vector2D _base, Vector2D _direction) {
		base = _base;
		direction = _direction;
	}
	
	public Vector2D getBase() {
		return base;
	}
	
	public Vector2D getDirection() {
		return direction;
	}
}
