package physics;

public class Range {
	
	private double min;
	private double max;
	
	public void setMin(double _min) { min = _min; }
	public void setMax(double _max) { max = _max; }
 	public double getMin() { return min; }
	public double getMax() { return max; }

	public void sort() {
		if (min > max) {
			double temp = min;
			min = max;
			max = temp;
		}
	}
	
	public static boolean overlappingRanges(Range r1, Range r2) {
		return r2.getMin() <= r1.getMax() && r1.getMin() <= r2.getMax();
	}
	
}
