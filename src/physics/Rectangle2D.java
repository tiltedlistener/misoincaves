package physics;

public class Rectangle2D {

	private Vector2D origin;
	private Vector2D size;
	
	public Rectangle2D(Vector2D _origin, Vector2D _size) {
		origin = _origin;
		size = _size;
	}
	
	public Vector2D getOrigin() {
		return origin;
	}
	
	public void setOrigin(Vector2D _vec) {
		origin = _vec;
	}
	
	public Vector2D getSize() {
		return size;
	}
	
	public void setSize(Vector2D _vec) {
		size = _vec;
	}
	
	public void translate(Vector2D _trans) {
		origin.add(_trans);
	}
	
	public Vector2D[] getCorners() {
		Vector2D[] corners = {origin, new Vector2D(origin.X() + size.X(), origin.Y()), new Vector2D(origin.X() + size.X(), origin.Y() + size.Y()), new Vector2D(origin.X(), origin.Y() + size.Y())};
		return corners;
	}
	
	public double minX() {
		return origin.X();
	}
	
	public double maxX() {
		return origin.X() + size.X();
	}
	
	public double minY() {
		return origin.Y();
	}
	
	public double max() {
		return origin.Y() + size.Y();
	}
	
	public double height() {
		return size.Y();
	}
	
	public double width() {
		return size.X();
	}
	
	public void enlarge(Rectangle2D _growth) {
		Vector2D maxCorner = Vector2D.addVectors(_growth.getOrigin(), _growth.getSize());
		Rectangle2D enlarged = enlargeRectanglePoint(maxCorner);
		Rectangle2D enlargedResult = enlarged.enlargeRectanglePoint(_growth.getOrigin());
		
		origin = enlargedResult.getOrigin();
		size = enlargedResult.getSize();
	}
	
	public Rectangle2D enlargeRectanglePoint(Vector2D p) {
		Rectangle2D enlarged;
		Vector2D enlargedOrigin = new Vector2D(
				Math.min(origin.X(), p.X()),
				Math.min(origin.Y(), p.Y())
				);
		Vector2D enlargedSize = new Vector2D(
				Math.max(origin.X() + size.X(), p.X()),
				Math.max(origin.Y() + size.Y(), p.Y())				
				);
		enlargedSize.sub(enlargedOrigin);
		
		enlarged = new Rectangle2D(enlargedOrigin, enlargedSize);
		return enlarged;
	}
	
}
