package physics;

public class Manifold {

	private Vector2D normal;
	private double penetration; 
	
	public void setNormal(Vector2D _n) {
		normal = _n;
	}
	
	public void setPenetration(double _p) {
		penetration = _p;
	}
	
	public Vector2D getNormal() {
		return normal;
	}
	
	public double getPenetration() {
		return penetration;
	}
	
}

