package physics;

public class Vector2D {

	private double x;
	private double y;
	
	public Vector2D() {
		x = 0;
		y = 0;
	}
	
	public Vector2D(double _x, double _y) {
		x = _x;
		y = _y;
	}
	
	public void setValues(double _x, double _y) {
		x = _x;
		y = _y;
	}
	
	public double X() {
		return x;
	}
	
	public double Y() {
		return y;
	}
	
	public void add(Vector2D vec) {
		x += vec.X();
		y += vec.Y();
	}
	
	public void sub(Vector2D vec) {
		x -= vec.X();
		y -= vec.Y();
	}
	
	public double length() {
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	}
	
	public Vector2D clone() {
		return new Vector2D(x, y);
	}
	
	public void invert() {
		x = -x;
		y = -y;
	}
	
	public void scale(double scale) {
		x *= scale;
		y *= scale;
	}
	
	public void scale(double scaleX, double scaleY) {
		x *= scaleX;
		y *= scaleY;
	}
	
	public boolean overlap(Vector2D vec) {
		if (x == vec.x && y == vec.y) return true;
		return false;
	}
	
	public boolean isZero() {
		if (x == 0 && y == 0) return true;
		return false;
	}
	
	public static Vector2D addVectors(Vector2D first, Vector2D second) {
		return new Vector2D(first.X() + second.X(), first.Y() + second.Y());
	}
	
	public static Vector2D subVectors(Vector2D first, Vector2D second) {
		return new Vector2D(first.X() - second.X(), first.Y() - second.Y());
	}
	
	public static Vector2D divideVector(Vector2D vec, double d) {
		return new Vector2D(vec.X() / d, vec.Y() / d);
	}
	
	public static double dotProduct(Vector2D first, Vector2D second) {
		return first.X() * second.X() + first.Y() * second.Y();
	}
	
	public static Vector2D rotateVector90(Vector2D vec) {
		return new Vector2D(-vec.Y(), vec.X());
	}
}
