package strategy;

import physics.Vector2D;
import entities.GameEntity;

public class MoveLeftStrategy implements GameEntityStrategy {
	
	public void run(GameEntity entity) {
		entity.setVelocity(new Vector2D(-5, 0));
	}

}
