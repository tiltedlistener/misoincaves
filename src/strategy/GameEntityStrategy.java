package strategy;

import entities.GameEntity;

public interface GameEntityStrategy {
	public void run(GameEntity entity);
}
