package particles;

import physics.Vector2D;
import components.GraphicsComponent;
import components.InputComponent;
import components.PhysicsComponent;
import entities.GameEntity;

public class Particle extends GameEntity {
	
	private ParticleGraphicsComponent graphics;
	
	private int lifespan;
	private int lifespanTicker = 0;
	
	public Particle(GraphicsComponent _graphics, PhysicsComponent _physics, InputComponent _input) {
		super(_graphics, null, null);
		graphics = (ParticleGraphicsComponent) _graphics;
		setSize(new Vector2D(5, 5));
		lifespan = (int) (Math.random() * 40);
	}
	
	public void setOrigin(Vector2D _pos, Vector2D _vel) {
		this.setPosition(_pos);
		this.setVelocity(_vel);
	}
	
	public void render(double interpolation) {
		graphics.render(this, interpolation);
	}
	
	@Override
	public void update() {
		updatePosition(getVelocity());
		lifespanTicker++;
		if (lifespanTicker > lifespan) {
			deactivate();
		}
	}

}
