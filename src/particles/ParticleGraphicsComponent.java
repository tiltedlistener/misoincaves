package particles;

import java.awt.Color;
import java.awt.Graphics2D;

import components.GraphicsComponent;

import game.Camera;

public class ParticleGraphicsComponent extends GraphicsComponent {

	public ParticleGraphicsComponent(Graphics2D _g, Camera _cam) {
		super(_g, _cam);
	}

	public void render(Particle particle, double interpolation) {
		g2d.setColor(Color.BLUE);
		g2d.fillRect((int)(particle.getPosition().X() - camera.getPosition().X()), (int)(particle.getPosition().Y() - camera.getPosition().Y()), (int)particle.getSize().X(), (int)particle.getSize().Y());
	}	
}
