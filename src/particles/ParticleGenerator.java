package particles;

import game.Camera;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import components.PhysicsComponent;
import physics.Vector2D;

public abstract class ParticleGenerator {
	
	protected Graphics2D g2d;
	protected Camera camera;
	protected LinkedList<Particle> particles = new LinkedList<Particle>();
	
	public abstract void generateParticles(Vector2D origin);	
	
	public ParticleGenerator(Graphics2D _g, Camera _cam) {
		g2d = _g;
		camera = _cam;
	}	
	
	public void update() {
		Iterator<Particle> particleIt = particles.iterator();
		ArrayList<Particle> inactive = new ArrayList<Particle>();
		while(particleIt.hasNext()) {
			Particle part = particleIt.next();
			part.update();
			
			if (!part.isActive()) {
				inactive.add(part);
			}
		}
		
		for (Particle part : inactive) {
			particles.remove(part);
		}
		inactive.clear();
	}
	
	public void render(double interpolation) {
		Iterator<Particle> particleIt = particles.iterator();
		while(particleIt.hasNext()) {
			particleIt.next().render(interpolation);
		}		
	}
	

}
