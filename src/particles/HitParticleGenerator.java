package particles;

import game.Camera;

import java.awt.Graphics2D;
import java.util.Random;

import physics.Vector2D;

public class HitParticleGenerator extends ParticleGenerator {

	private Random rand = new Random();
	private boolean processing = false;
	
	public HitParticleGenerator(Graphics2D _g, Camera _cam) {
		super(_g, _cam);
	}
	
	@Override
	public void generateParticles(Vector2D origin) {	
		particles.clear();
		for (int i = 0; i < 20; i++) {
			double initialXSpeed = rand.nextDouble() * 10;
			double initialYSpeed = rand.nextDouble() * 10;	
			
			Vector2D vel;
			if (i < 5) {
				vel = new Vector2D(initialXSpeed, initialYSpeed);
			} else if (i > 4 && i < 10) {
				vel = new Vector2D(-initialXSpeed, initialYSpeed);
			} else if (i > 9 && i < 15) {
				vel = new Vector2D(-initialXSpeed, -initialYSpeed);
			} else {
				vel = new Vector2D(initialXSpeed, -initialYSpeed);
			}
			
			particles.add(new Particle(new ParticleGraphicsComponent(g2d, camera), null, null));	
			particles.get(i).setOrigin(origin.clone(), vel);
		}	
			
	}	

}
