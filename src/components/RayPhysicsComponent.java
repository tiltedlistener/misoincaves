package components;

import physics.Ray;
import entities.*;
import game.SceneGraph;

public class RayPhysicsComponent extends PhysicsComponent {
	
	public RayPhysicsComponent(SceneGraph _world) {
		super(_world);
	}
	
	public GameEntity castRay(Ray ray) {
		return world.checkRayCollision(ray);
	}
	
}
