package components;

import physics.Vector2D;
import input.InputHandler;
import input.InputInfo;
import entities.GameEntity;

public class InputComponent {
	
	private InputHandler handler;
	
	public InputComponent(InputHandler _handler) {
		handler = _handler;
	}
	
	public void update(GameEntity _sprite) {
		InputInfo input = handler.handleInput();
		if (input != null) {
			switch(input) {
			case DARROW_PRESSED:
				_sprite.setVelocity(new Vector2D(0, 5));
				break;
			case UARROW_PRESSED:
				_sprite.setVelocity(new Vector2D(0, -5));
				break;			
			case UARROW_RELEASED:
			case DARROW_RELEASED:
				_sprite.setVelocity(new Vector2D(0, 0));			
				break;
			case LARROW_PRESSED:
				_sprite.setVelocity(new Vector2D(-10, 0));
				break;
			case RARROW_PRESSED:
				_sprite.setVelocity(new Vector2D(10, 0));
				break;			
			case LARROW_RELEASED:
			case RARROW_RELEASED:
				_sprite.setVelocity(new Vector2D(0, 0));			
				break;				
			default:
				break;
			}			
		}
	}
}
