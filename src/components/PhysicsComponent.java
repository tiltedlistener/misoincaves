package components;

import physics.Sweep;
import physics.Vector2D;
import entities.*;
import game.SceneGraph;

public class PhysicsComponent {
	
	protected SceneGraph world;
	protected Sweep sweep;
	
	public PhysicsComponent(SceneGraph _world) {
		world = _world;
	}	
	
	public void applySweep(Sweep _sweep) {
		sweep = _sweep;
	}
	
	public void update(GameEntity _self) {
		_self.updatePosition(_self.getVelocity());
	}
	
	public void resolveCollision(GameEntity _self, Sweep resolution) {
		if (resolution.time != 1.0) {	
			resolution.normal.scale(resolution.time);
			Vector2D vel = _self.getVelocity().clone();
			vel.scale(resolution.normal.X(), resolution.normal.Y());
			_self.updatePosition(vel);
		}
	}
}
