package components;

import java.awt.Graphics2D;

import particles.ParticleGenerator;
import physics.Vector2D;
import entities.GameEntity;
import game.Camera;

public class GraphicsComponent {
	
	protected Graphics2D g2d;
	protected Camera camera;
	
	public GraphicsComponent(Graphics2D _g, Camera _cam) {
		g2d = _g;
		camera = _cam;
	}
	
	public void render(GameEntity _sprite, double interpolation) {
		g2d.fillRect(
				(int)((_sprite.getPosition().X() - _sprite.getLastPosition().X()) * interpolation + _sprite.getLastPosition().X() - camera.getPosition().X()), 
				(int)((_sprite.getPosition().Y() - _sprite.getLastPosition().Y()) * interpolation + _sprite.getLastPosition().Y() - camera.getPosition().Y()), 
				(int)(_sprite.getSize().X()), 
				(int)(_sprite.getSize().Y()));		
	}
	
}
