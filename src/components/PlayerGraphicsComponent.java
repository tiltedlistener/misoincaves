package components;

import entities.GameEntity;
import game.Camera;

import java.awt.Color;
import java.awt.Graphics2D;

import particles.HitParticleGenerator;
import physics.Vector2D;

public class PlayerGraphicsComponent extends GraphicsComponent {
	
	private HitParticleGenerator particles;
	
	public PlayerGraphicsComponent(Graphics2D _g, Camera _cam) {
		super(_g, _cam);
		particles = new HitParticleGenerator(_g, _cam);
	}
	
	public void update() {
		particles.update();
	}

	public void render(GameEntity _sprite, double interpolation) {
		g2d.setColor(Color.BLUE);
		super.render(_sprite, interpolation);
		particles.render(interpolation);
	}	
	
	public void generateParticles(GameEntity entity, Vector2D direction) {
		Vector2D pos = entity.getPosition().clone();

		if (direction.Y() == 0) {
			if (direction.X() < 0) {
				pos.add(new Vector2D(50, 25));
			} else {
				pos.add(new Vector2D(0, 25));
			}
		} else {
			System.out.println("HERE!");			// This is never firing, so something's up
			if (direction.Y() < 0) {
				pos.add(new Vector2D(25, 0));
			} else {
				pos.add(new Vector2D(25, 50));
			}
		}
		particles.generateParticles(pos);
	}
	
}
