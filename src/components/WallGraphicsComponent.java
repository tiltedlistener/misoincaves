package components;

import java.awt.Color;
import java.awt.Graphics2D;

import entities.GameEntity;
import game.Camera;

public class WallGraphicsComponent extends GraphicsComponent {

	public WallGraphicsComponent(Graphics2D _g, Camera _cam) {
		super(_g, _cam);
	}

	public void render(GameEntity _sprite, double interpolation) {
		g2d.setColor(Color.GRAY);
		super.render(_sprite, interpolation);
	}	
	
}
