package input;

public class InputKey {

	private InputInfo downType;
	private InputInfo upType;
	private int keyCode;
	private boolean keyPressed = false;
	
	public InputKey(int _keyCode, InputInfo _dType, InputInfo _uType) {
		downType = _dType;
		upType = _uType;
		keyCode = _keyCode;
	}
	
	public InputInfo applyKeyPress(int code) {
		if (!keyPressed && code == keyCode) {
			keyPressed = true;
			return downType;
		}
		return null;
	}
	
	public InputInfo applyKeyRelease(int code) {
		if (code == keyCode) {
			keyPressed = false;
			return upType;
		}
		return null;
	}
	
}
