package command;

import game.Event;
import physics.Vector2D;

public class Command {
	/**
	 * For now we are assuming that Commands only relate to events
	 */
	
	private Vector2D vec;
	private Event event;
	
	public void setEvent(Event _event) {
		event = _event;
	}
	
	public Event getEvent() {
		return event;
	}
	
	public Vector2D getVector2D() {
		return vec;
	}
	
	public void setVector2D(Vector2D _vec) {
		vec = _vec;
	}
}
