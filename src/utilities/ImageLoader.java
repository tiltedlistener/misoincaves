package utilities;

import java.awt.Image;
import java.awt.Toolkit;

public class ImageLoader {
	
	public static Image loadImage(String filename) {
		Toolkit tk = Toolkit.getDefaultToolkit();
		return tk.getImage(filename);	
	}
	
}
