package entities;

import physics.Ray;
import strategy.GameEntityStrategy;
import components.*;

public class FollowingEntity extends GameEntity {
	
	private Ray ray;
	private RayPhysicsComponent physics;
	private GameEntityStrategy followStrat;	

	public FollowingEntity(GraphicsComponent _graphics, RayPhysicsComponent _physics) {
		super(_graphics, _physics, null);
		mass = 10;
		physics = (RayPhysicsComponent) _physics;
		
		ray = new Ray();
		ray.setDistance(300);
		ray.addMask("class entities.FollowingEntity");
		ray.addMask("class entities.WallEntity");
	}
	
	public void addFollowStrat(GameEntityStrategy strat) {
		followStrat = strat;
	}
	
	@Override
	public void update() {
		lastPosition = pos.clone();
		physics.update(this);			
		updateRay();
	}
	
	public void updateRay() {
		ray.setPosition(getPosition());
		if (physics.castRay(ray) != null) {
			followStrat.run(this);
		}
	}

}
