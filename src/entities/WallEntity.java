package entities;

import physics.Vector2D;
import components.GraphicsComponent;
import components.PhysicsComponent;

public class WallEntity extends GameEntity {
	
	public WallEntity(GraphicsComponent _graphics, PhysicsComponent _physics) {
		super(_graphics, _physics, null);
		mass = Double.MAX_VALUE;
		size = new Vector2D(50, 50);
		vel = new Vector2D(0, 0);
		elasticity = 0.05;
	}
	
	@Override
	public void update() {
		lastPosition = pos.clone();
		physics.update(this);		
	}

}
