package entities;

import physics.Rectangle2D;
import physics.Vector2D;

public abstract class BaseEntity {

	protected Vector2D pos;
	protected Vector2D lastPosition;
	protected Vector2D vel;
	protected Vector2D size;
	protected double rotation;
	protected boolean active = true;
	
	public abstract void update();
	
	public Vector2D getPosition() {
		return pos;
	}
	
	public void setPosition(Vector2D _vec) {
		pos = _vec;
	}
	
	public void adjustPosition(Vector2D _vec) {
		if (lastPosition == null) lastPosition = new Vector2D(0,0);	
		else lastPosition = pos;
		pos.add(_vec);
	}
	
	public void updatePosition(Vector2D _vec) {
		lastPosition = pos;
		pos.add(_vec);
	}
	
	public Vector2D getLastPosition() {
		if (lastPosition == null) return new Vector2D(0,0);
		return lastPosition;
	}
	
	public boolean isMoving() {
		return false;
	}
	
	public Vector2D getVelocity() {
		return vel;
	}
	
	public void setVelocity(Vector2D _vec) {
		vel = _vec;
	}
	
	public void adjustVelocity(Vector2D _vec) {
		vel.add(_vec);
	}
	
	public void stop() {
		vel = new Vector2D(0, 0);
	}
	
	public void setSize(Vector2D _vec) {
		size = _vec;
	}
	
	public Vector2D getSize() {
		return size;
	}
	
	public Rectangle2D getRectangle() {
		return new Rectangle2D(getPosition().clone(), getSize().clone());
	}
	
	public void updateVelocity(Vector2D _vec) {
		vel.add(_vec);
	}
	
	public double getRotation() {
		return rotation;
	}
	
	public void setRotation(double deg) {
		rotation = deg;
	}
	
	public boolean isActive() {
		return active;
	}
	
	public void activate() {
		active = true;
	}
	
	public void deactivate() {
		active = false;
	}
}
