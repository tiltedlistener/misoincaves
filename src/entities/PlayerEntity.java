package entities;

import game.Event;

import java.util.ArrayList;

import physics.Sweep;
import physics.Vector2D;
import serviceintefaces.Observer;
import serviceintefaces.Subject;
import command.Command;
import components.*;

public class PlayerEntity extends GameEntity implements Subject {
	
	private ArrayList<Observer> observers;
	
	public PlayerEntity(GraphicsComponent _graphics, PhysicsComponent _physics, InputComponent _input) {
		super(_graphics, _physics, _input);
		graphics = (PlayerGraphicsComponent) _graphics;
		observers = new ArrayList<Observer>();
	}
	
	@Override
	public void addObserver(Observer obs) {
		observers.add(obs);
	}

	@Override
	public void removeObserver(Observer obs) {
		observers.remove(obs);
	}

	@Override
	public void notifyObservers(Command command) {
		for (Observer obs : observers) {
			obs.onNotify(command);
		}
	}

	@Override
	public void update() {
		super.update();
		((PlayerGraphicsComponent) graphics).update();
	}
	
}
