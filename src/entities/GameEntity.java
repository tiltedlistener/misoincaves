package entities;

import physics.Sweep;
import physics.Vector2D;
import components.GraphicsComponent;
import components.InputComponent;
import components.PhysicsComponent;

public class GameEntity extends BaseEntity {
	
	protected GraphicsComponent graphics;
	protected PhysicsComponent physics;
	protected InputComponent input;
	
	protected double mass = 10;
	protected double elasticity = 0.05;		
	
	public GameEntity(GraphicsComponent _graphics, PhysicsComponent _physics, InputComponent _input) {
		graphics = _graphics;
		physics = _physics;
		input = _input;
		size = new Vector2D(25, 25);
	}
	
	@Override
	public boolean isMoving() {
		return true;
	}

	@Override
	public void update() {
		lastPosition = pos.clone();
		input.update(this);
		physics.update(this);
	}

	public void render(double interpolation) {
		graphics.render(this, interpolation);
	}
	
	public void resolveCollision(Sweep resolution) {
		physics.resolveCollision(this, resolution);
	}
	
	public double getMass() {
		return mass;
	}
	
	public double getElasticity() {
		return elasticity;
	}
		
}
