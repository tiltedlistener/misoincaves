package factories;

import game.Camera;
import game.SceneGraph;
import input.InputHandler;

import java.awt.Graphics2D;
import java.util.ArrayList;

import particles.ParticleGenerator;
import physics.Vector2D;
import components.*;
import entities.*;

public class GameEntityFactory {
	
	private Graphics2D g2d;
	private InputHandler handler;
	private SceneGraph world;
	private Camera camera;
	
	public GameEntityFactory(Graphics2D _g, InputHandler _handler, SceneGraph _graph, Camera _cam) {
		g2d = _g;
		handler = _handler;
		world = _graph;
		camera = _cam;
	}
	
	public GameEntity createPlayer() {
		return new PlayerEntity(new PlayerGraphicsComponent(g2d, camera), new PhysicsComponent(world), new InputComponent(handler));
	}

	public WallEntity createWall() {
		return new WallEntity(new WallGraphicsComponent(g2d, camera), new PhysicsComponent(world));
	}	
	
	public GameEntity createFollowing() {
		return new FollowingEntity(new FollowingGraphicsComponent(g2d, camera), new RayPhysicsComponent(world));		
	}
	
	public ArrayList<WallEntity> createWallLine(boolean horizontal, int count, double startX, double startY) {
		ArrayList<WallEntity> group = new ArrayList<WallEntity>();
		for (int i = 0; i < count; i++) {
			WallEntity current = createWall();
			if (horizontal) {
				current.setPosition(new Vector2D(startX + i * (current.getSize().X()), startY ));
			} else {
				current.setPosition(new Vector2D(startX, startY + i *( current.getSize().Y()) ));
			}
			group.add(current);
		}
		return group;
	}
	
}

